package com.devcamp.voucher.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.models.CVoucher;
import com.devcamp.voucher.repositories.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
            pVoucherRepository.findAll().forEach(pVouchers::add);

			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		Optional<CVoucher> voucherData = this.pVoucherRepository.findById(id);

		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @PostMapping("/vouchers")
	public ResponseEntity<CVoucher> createCVoucher(@RequestBody CVoucher pVouchers) {
		try {
			CVoucher voucher = new CVoucher();
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setNgayCapNhat(new Date());
            voucher.setNgayTao(new Date());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());

            pVoucherRepository.save(voucher);

			return new ResponseEntity<>(voucher, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
        try {
            Optional<CVoucher> voucherData = this.pVoucherRepository.findById(id);

            if (voucherData.isPresent()) {
                CVoucher voucher = voucherData.get();
                voucher.setGhiChu(pVouchers.getGhiChu());
                voucher.setMaVoucher(pVouchers.getMaVoucher());
                voucher.setNgayCapNhat(new Date());
                voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());

                return new ResponseEntity<>(pVoucherRepository.save(pVouchers), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
    	} catch (Exception e) {
			System.out.println(e); 
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
            Optional<CVoucher> voucherData = this.pVoucherRepository.findById(id);

            if (voucherData.isPresent()) {
                this.pVoucherRepository.delete(voucherData.get());

                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @DeleteMapping("/vouchers")
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			this.pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
